# modules/vpc-peer/main.tf

# connect to primary region
provider "aws" {
  region = var.source_region
}


# connect to secondary region
provider "aws" {
  region = var.peer_region
  alias = "peer"
}

data "aws_caller_identity" "peer" {
  provider = aws.peer
}

# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer" {
  peer_owner_id = data.aws_caller_identity.peer.account_id
  peer_vpc_id   = var.peer_region_aws_vpc_id
  vpc_id        = var.source_region_aws_vpc_id
  peer_region   = var.peer_region
  auto_accept   = false

  tags = {
    Side = "Requester"
    SE_Initials = var.ntnx_se_initials
    Terraform   = "True"
  }
}

# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
    SE_Initials = var.ntnx_se_initials
    Terraform   = "True"
  }
}

resource "aws_route" "peer_side_vpc_route" {
  route_table_id            = var.source_region_private_rt_id
  destination_cidr_block    = var.peer_region_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}

resource "aws_route" "source_side_vpc_route" {
  provider                  = aws.peer
  route_table_id            = var.peer_region_private_rt_id
  destination_cidr_block    = var.source_region_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}

# modules/vpc/main.tf


# connect to correct region
provider "aws" {
  region = var.region
}


# get region availability zones
data "aws_availability_zones" "available" {}


# create vpc
resource "aws_vpc" "main" {
  cidr_block       = var.aws_region_cidr
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = false

  tags = {
    SE_Initials = var.ntnx_se_initials
    Terraform   = "True"
    Name        = "${var.ntnx_se_initials}-vpc"
  }
}

output "vpc_id" {
  value       = aws_vpc.main.id
  description = "vpc id"
}

# create internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.ntnx_se_initials}-igw"
  }
}


# create vpn gateway
resource "aws_vpn_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.ntnx_se_initials}-vpngw"
  }
}


resource "aws_customer_gateway" "main" {
  bgp_asn    = 65000
  ip_address = var.on_prem_public_ip
  type       = "ipsec.1"

  tags = {
    SE_initials = var.ntnx_se_initials
    Terraform   = "True"
    Name        = "${var.ntnx_se_initials}-cgw"
  }
}

resource "aws_vpn_connection" "main" {
  vpn_gateway_id      = aws_vpn_gateway.main.id
  customer_gateway_id = aws_customer_gateway.main.id
  type                = "ipsec.1"
  static_routes_only  = var.static_routing

  tags = {
    SE_initials = var.ntnx_se_initials
    Terraform   = "True"
    Name        = "${var.ntnx_se_initials}-vpn"
  }
}

# create public subnets
resource "aws_subnet" "public" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "${var.aws_region_ip_prefix}.${count.index}.0/24"
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    SE_initials = var.ntnx_se_initials
    Terraform   = "True"
    Name        = "${var.ntnx_se_initials}-${data.aws_availability_zones.available.names[count.index]}-public"
  }
}


# create single nat gateway for vpc
resource "aws_eip" "nat" {
  vpc      = true
}
resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public[0].id
  #vpc_id        = aws_vpc.main.id

  tags = {
    Name = "${var.ntnx_se_initials}-natgw"
  }
}


# Create route table to route public network traffic to internet gateway
resource "aws_route_table" "public" {
  vpc_id        = aws_vpc.main.id
  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.main.id
  }

  tags = {
    Name = "${var.ntnx_se_initials}-public-rt"
  }
}


# Associate public networks with the public route table
resource "aws_route_table_association" "public" {
  count           = length(aws_subnet.public)
  subnet_id       = aws_subnet.public[count.index].id
  route_table_id  = aws_route_table.public.id
}

# create private subnets for cvm mgmt and uvms
locals {
  product = setproduct(var.aws_region_private_networks, data.aws_availability_zones.available.names)
}

resource "aws_subnet" "private" {
  count                   = length(var.aws_region_private_networks) * length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "${var.aws_region_ip_prefix}.${10+count.index}.0/24"
  availability_zone       = element(local.product, count.index)[1]
  map_public_ip_on_launch = false

  tags = {
    SE_Initials = var.ntnx_se_initials
    Terraform   = "True"
    Name        = "${var.ntnx_se_initials}-${element(local.product, count.index)[0]}-${element(local.product, count.index)[1]}"
  }
}


# Create route table to route private network traffic to nat gatweway
resource "aws_route_table" "private" {
  vpc_id            = aws_vpc.main.id
  propagating_vgws  = [aws_vpn_gateway.main.id]

  tags = {
    Name = "${var.ntnx_se_initials}-private-rt"
  }
}

output "private_rt_id" {
  value       = aws_route_table.private.id
  description = "private route table id"
}


# Associate private networks with the private route table
resource "aws_route_table_association" "private" {
  count           = length(aws_subnet.private)
  subnet_id       = aws_subnet.private[count.index].id
  route_table_id  = aws_route_table.private.id
}


# Add route to allow private networks access to the nat gateway
resource "aws_route" "nat-gateway" {
  route_table_id            = aws_route_table.private.id
  destination_cidr_block    = "0.0.0.0/0"
  nat_gateway_id            = aws_nat_gateway.main.id
  depends_on                = [aws_route_table.private]
}


# Add route to private network
resource "aws_route" "on-prem" {
  count                     = var.static_routing == "true" ? 1 : 0
  route_table_id            = aws_route_table.private.id
  destination_cidr_block    = var.on_prem_subnet
  gateway_id                = aws_vpn_gateway.main.id
  depends_on                = [aws_route_table.private]
}

variable "region" {}
variable "aws_region_cidr" {}
variable "ntnx_se_initials" {}
variable "aws_region_ip_prefix" {}
variable "aws_region_private_networks" {}
variable "on_prem_public_ip" {}
variable "static_routing" {}
variable "on_prem_subnet" {}

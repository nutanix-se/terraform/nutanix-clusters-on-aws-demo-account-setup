# main.tf

module "primary_vpc" {
  source                      = "./modules/vpc"
  region                      = var.aws_primary_region
  aws_region_cidr             = var.aws_region_network_cidr[var.aws_primary_region]
  aws_region_ip_prefix        = var.aws_region_network_prefix[var.aws_primary_region]
  aws_region_private_networks = var.aws_region_private_networks
  ntnx_se_initials            = var.ntnx_se_initials
  on_prem_public_ip           = var.on_prem_public_ip
  static_routing              = var.static_routing
  on_prem_subnet              = var.on_prem_subnet
}

module "secondary_vpc" {
  source                      = "./modules/vpc"
  region                      = var.aws_secondary_region
  aws_region_cidr             = var.aws_region_network_cidr[var.aws_secondary_region]
  aws_region_ip_prefix        = var.aws_region_network_prefix[var.aws_secondary_region]
  aws_region_private_networks = var.aws_region_private_networks
  ntnx_se_initials            = var.ntnx_se_initials
  on_prem_public_ip           = var.on_prem_public_ip
  static_routing              = var.static_routing
  on_prem_subnet              = var.on_prem_subnet
}

module "vpc_peer" {
  source                      = "./modules/vpc-peer"
  source_region               = var.aws_primary_region
  source_region_aws_vpc_id    = module.primary_vpc.vpc_id
  source_region_cidr          = var.aws_region_network_cidr[var.aws_primary_region]
  source_region_private_rt_id = module.primary_vpc.private_rt_id
  peer_region                 = var.aws_secondary_region
  peer_region_aws_vpc_id      = module.secondary_vpc.vpc_id
  peer_region_cidr            = var.aws_region_network_cidr[var.aws_secondary_region]
  peer_region_private_rt_id   = module.secondary_vpc.private_rt_id
  ntnx_se_initials            = var.ntnx_se_initials
}








# Create route53 private DNS for name resolution

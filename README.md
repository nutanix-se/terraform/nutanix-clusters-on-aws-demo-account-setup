# Nutanix Clusters on AWS - Demo Account Setup
This Terraform module creates everything needed within AWS to demo Nutanix clusters.

Main Features:
- All entity names prefixed with your initials to ensure uniqueness.
- Creates VPC, subnets and routing across two AWS regions including a VPC peer relationship.
- Configures Internet gateway, NAT gateway and routing to allow private subnets to access the internet.
- Configures everything required to establish a VPN connection from your a home lab.

What this does not do:
- Configure the VPN / routing between your private lab and AWS. You'll have to add the VPN/BGP configuration to your home router.
	- If you use a router capable of ipsec VPN & BGP you can download viable configurations directly from AWS.
		- When logged into the AWS console click on Services > VPC > Site-to-Site VPN Connections. Select the generated VPN connection and click on the 'Download Configuration' button. This will generate configs for most routers that you can use.
	- If you do not have a router capable of ipsec VPN & BGP consider using pfSense running as a VM in your home lab.
		- https://fusion1337.medium.com/setup-site-to-site-vpn-to-aws-with-pfsense-1cac16623bd6
- Configure the UVM security group to allow you to access deployed VMs in Nutanix Clusters.
	- Once you have deployed your cluster(s) reference this page of the docs to make the necessary security changes https://portal.nutanix.com/page/documents/details?targetId=Nutanix-Clusters-AWS:aws-clusters-aws-user-vm-security-c.html
- Any post-deployment configuration of the Nutanix on AWS cluster like;
	- Cluster Virtual IP / Data Services IP
	- Containers
	- VM Networks
	- Protection Domains
	- etc.


## What is Nutanix Clusters?
Nutanix Clusters delivers the industry’s first cloud platform with the flexibility, simplicity, and cost efficiency needed to manage applications and infrastructure in private and multiple public clouds, operated as a single cloud.

Nutanix Clusters dramatically reduces the operational complexity of migrating, extending or bursting your applications and data between clouds. You can use a single management plane to manage both your Nutanix private cloud and your public cloud infrastructure. Easily extend the full Nutanix stack to public clouds like AWS and more!

For more details head to the Nutanix website https://www.nutanix.com/products/clusters.

## Getting Started
To use this module you will need to first install and configre terraform & AWS CLI.
- Terraform https://www.terraform.io/
- AWS Cli https://aws.amazon.com/cli/

On a Mac you can use brew to simplify and automate the installation. For more info on the brew package manager head to their website https://brew.sh/.

On a Windows PC you can conversely use Chocolatey to automate the installation. For more info on the Chocolatey package manager head to their website https://chocolatey.org/.


## Installation
### Mac
1. Install Brew
```shell
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
2. Install AWS CLI, Terraform and Git
```shell
brew install terraform awscli git
```


### Windows
1. Install Chocolatey
	a. Launch an administrative powershell session.
	b. Ensure Get-ExecutionPolicy is not Restricted.
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```
	c. Copy the script and paste it into your shell from https://chocolatey.org/install.ps1
2. Use Chocolatey to install AWS CLI, Terraform and Git
```shell
choco install terraform awscli git -y
```


## Configuration
1. Create an AWS access key
	1. Using a web browser access your AWS console.
	2. Click on Services > IAM (Under 'Security, Identity, & Compliance').
	3. Click on Users.
	4. Click the button labelled 'Add User'.
	5. Enter a username eg. 'terraform' and select 'Programmatic access' for the access type.
	6. Select the existing 'Admin' group
	7. Click the 'Next' button twice to pass through the 'Tags' and 'Review' screens.
	8. Click on the 'Create User' button.
	9. Make a note of the 'Access key ID' and 'Secret access key'

2. At a command prompt typr `aws configure` and provide the 'Access key ID' and 'Secret access key' to configure the AWS CLI. For example;
```shell
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]:
Default output format [None]:
```

3. Clone the gitlab repositary which contains the terraform module
```shell
cd to-base-directory
git clone https://gitlab.com/nutanix-se/terraform/nutanix-clusters-on-aws-demo-account-setup
cd nutanix-clusters-on-aws-demo-account-setup
```

4. Initialize terraform to download required core terraform modules.
```shell
terraform init
```

5. Use Terraform to configure your AWS account.
	> When running either plan or apply you will be prompted for a number of variables. Please follow the input descriptions and provide the relevant data.

	1. View the changes that terraform will make to your AWS account;
	```shell
	terraform plan
	```
	2. Execute the module to create all the necessary AWS components
	```shell
	terraform apply
	```
	3. At the end of your demo remember to tear down the configuration to stop incurring charges.
	```shell
	terraform destroy
	```

- If you would like to ovveride any of the default variables you can do so via the command line (or by editing the variables.tf file).

```shell
terraform plan -var="static_routing=true" -var="aws_primary_region=us-west-2" -var="aws_secondary_region=us-east-2"
```

## What next?
Hop over to the Nutanix portal at https://my.nutanix.com and configure your first AWS cluster. Documentation on how to do this is available here https://portal.nutanix.com/page/documents/details?targetId=Nutanix-Clusters-AWS:aws-clusters-aws-getting-started-c.html.

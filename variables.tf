variable "ntnx_se_initials" {
  description = "Nutanix SE Initials to name the objects created"
  type        = string
}

variable "on_prem_public_ip" {
  description = "On-prem IPv4 address of your router. https://whatismyipaddress.com/"
  type        = string
}

variable "on_prem_subnet" {
  description = "Subnet CIDR for the on-prem network in which your Nutanix cluster resides. For example 192.168.1.0/24"
  type        = string
}

variable "static_routing" {
  description = "Whether to use static routing instead of BGP for the ipsec VPN tunnel"
  type        = string
  default     = "false"
}

variable "aws_primary_region" {
  description = "Primary region"
  type        = string
  default     = "us-west-1"
}

variable "aws_secondary_region" {
  description = "Secondary Region"
  type        = string
  default     = "us-west-2"
}

variable "aws_region_network_cidr" {
  description = "The port the server will use for HTTP requests"
  type        = map
  default = {
    "us-west-1"  = "10.0.0.0/16"
    "us-west-2"  = "10.1.0.0/16"
    "us-east-1"  = "10.2.0.0/16"
    "us-east-2"  = "10.3.0.0/16"
  }
}

variable "aws_region_network_prefix" {
  description = "The port the server will use for HTTP requests"
  type        = map
  default = {
    "us-west-1"  = "10.0"
    "us-west-2"  = "10.1"
    "us-east-1"  = "10.2"
    "us-east-2"  = "10.3"
  }
}

variable "aws_region_private_networks" {
  type    = list
  default = ["mgmt", "uvm"]
}

variable "aws_region_private_network_count" {
  description = "The type and count of private networks"
  type        = map
  default = {
    "uvm" = 1
    "mgmt" = 1
  }
}
